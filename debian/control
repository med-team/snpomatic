Source: snpomatic
Maintainer: Debian Med Packaging Team <debian-med-packaging@lists.alioth.debian.org>
Uploaders: Sascha Steinbiss <satta@debian.org>
Section: science
Priority: optional
Build-Depends: debhelper-compat (= 13),
               asciidoctor,
               antiword
Standards-Version: 4.6.2
Vcs-Browser: https://salsa.debian.org/med-team/snpomatic
Vcs-Git: https://salsa.debian.org/med-team/snpomatic.git
Homepage: https://github.com/magnusmanske/snpomatic
Rules-Requires-Root: no

Package: snpomatic
Architecture: any
Depends: ${shlibs:Depends},
         ${misc:Depends}
Description: fast, stringent short-read mapping software
 High throughput sequencing technologies generate large amounts of short reads.
 Mapping these to a reference sequence consumes large amounts of processing
 time and memory, and read mapping errors can lead to noisy or incorrect
 alignments.
 .
 SNP-o-matic is a fast, stringent short-read mapping software. It supports a
 multitude of output types and formats, for uses in filtering reads, alignments,
 sequence-based genotyping calls, assisted reassembly of contigs etc.
